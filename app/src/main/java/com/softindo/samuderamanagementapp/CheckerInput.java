package com.softindo.samuderamanagementapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.softindo.samuderamanagementapp.Database.Const;
import com.softindo.samuderamanagementapp.Database.DBHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CheckerInput extends AppCompatActivity {

    /* OK HTTP */
    OkHttpClient client;
    JSONObject jsonObject;

    ProgressDialog progress;

    EditText berat, packing, jumlah_colly;
    Button simpan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checker_input);
        simpan  = (Button) findViewById(R.id.btn_simpan);
        berat   = (EditText) findViewById(R.id.berat);
        packing = (EditText) findViewById(R.id.packing);
        jumlah_colly = (EditText) findViewById(R.id.jumlah_colly);
        berat.requestFocus();


        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execSimpan();
            }
        });
    }

    private void execSimpan(){
        final Float v_berat           = Float.parseFloat(berat.getText().toString());
        final String v_packing        = packing.getText().toString();
        final Integer v_jumlah_colly  = Integer.parseInt(jumlah_colly.getText().toString());
        final Integer cabang = Integer.parseInt(DBHandler.getUser(getApplicationContext()).get(0).get("cabang"));
        progress = ProgressDialog.show(CheckerInput.this, "Proccessing",
                "Loading ..", true);
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        doSave(v_berat, v_packing, v_jumlah_colly, cabang);
                    }
                },
                1000);
    }

    private void doSave(Float berat, String packing, Integer jumlah_colly, Integer cabang){
        client = new OkHttpClient();

        RequestBody requestBody = new FormBody.Builder()
                .add("berat", String.valueOf(berat))
                .add("packing", packing)
                .add("jumlah_colly", String.valueOf(jumlah_colly))
                .add("cabang", String.valueOf(cabang))
                .build();

        final Request request = new Request.Builder()
                .url(Const.checkerInput)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed Input Data", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code : " + response);
                } else {
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.dismiss();
//                                Toast.makeText(getApplicationContext(), jsonObject + "", Toast.LENGTH_LONG).show();
//                                Log.d("object", jsonObject + "");
                                try {
                                    if (jsonObject.getInt("status") == 1) {
                                        execSuccess(jsonObject);
                                    } else {
                                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void execSuccess(JSONObject jsonObject){
        try {
            Integer success    = jsonObject.getInt("status");
            if(success == 1){
                String  message = jsonObject.getString("message");
                Integer stt    = jsonObject.getInt("stt");
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Intent showstt  = new Intent(getApplicationContext(), Checker_Show_Stt.class);
                showstt.putExtra("stt", stt);
                startActivity(showstt);
                finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_checker_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
