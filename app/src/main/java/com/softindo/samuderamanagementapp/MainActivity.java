package com.softindo.samuderamanagementapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.softindo.samuderamanagementapp.Database.DBHandler;

import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkAuth();
        context = getApplicationContext();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.logout){
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkAuth(){
        if(!DBHandler.checkUser(getApplicationContext())){
            Intent login = new Intent(getApplicationContext(), Login.class);
            startActivity(login);
            finish();
        }
        else{
            checkRole();
        }
    }

    private void checkRole(){
        List<Map<String, String>> user = DBHandler.getUser(getApplicationContext());
        String role = user.get(0).get("role");
        switch(role){
            case "checker" :
                Intent checker  = new Intent(getApplicationContext(), Checker.class);
                startActivity(checker);
                finish();
                break;
        }
    }

    public void logout(){
        if(DBHandler.deleteUser(getApplicationContext())){
            checkAuth();
        }
    }

}
