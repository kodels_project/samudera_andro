package com.softindo.samuderamanagementapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Permana on 1/27/2017.
 */
public class DBHandler {
    static int regId;
    static SQLiteDatabase db;
    static Cursor data;

    public static boolean checkUser(Context context){
        boolean cek = false;
        db = (new Database(context)).getWritableDatabase();
        data = db.rawQuery("SELECT * FROM user", null);
        Log.d("data", data.getCount() + "");
        if(data.getCount() > 0){
            cek = true;
        }
        return cek;
    }

    public static boolean registerUser(Context context, Map<String, String> queryValues){
//        boolean cek     = DBHandler.checkUser(context, queryValues.get("id_user"));

        db = (new Database(context)).getWritableDatabase();
        ContentValues values = new ContentValues();
        for(String key : queryValues.keySet()){
            values.put(key, queryValues.get(key));
        }
        db.insert("user", null, values);
        db.close();

        return true;
    }

    public static boolean deleteUser(Context context){
        db = (new Database(context)).getWritableDatabase();
        db.execSQL("DELETE FROM user");
        db.close();

        return true;
    }

    public static List<Map<String, String>> getUser(Context context){
        List<Map<String,String>> res = new ArrayList<Map<String, String>>();

        db = (new Database(context)).getWritableDatabase();
        data = db.rawQuery("SELECT * FROM user", null);
        if(data.moveToFirst()){
            while(!data.isAfterLast())
            {
                int i = 0;
                Map<String, String> cols = new HashMap<String, String>();
                for (String colname : data.getColumnNames())
                {
                    cols.put(data.getColumnName(i), data.getString(i));
                    ++i;
                }

                res.add(cols);
                data.moveToNext();
            }
        }

        return res;
    }

}
