package com.softindo.samuderamanagementapp.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;

/**
 * Created by Permana on 1/27/2017.
 */
public class Database extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "dyrecs";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS user(_id INTEGER PRIMARY KEY AUTOINCREMENT, id INTEGER, name TEXT, email TEXT, password TEXT, cabang INTEGER, role TEXT)");
    }

    //fungsi cursor
    public Cursor get(String query){
        return(getReadableDatabase().rawQuery(query,null));
    }

    //fungsi exec
    public void exec(String query) throws IOException {
        getWritableDatabase().execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        android.util.Log.w("Constants", "Upgrading database, which will destroy all old data");
    }
}
