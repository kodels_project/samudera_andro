package com.softindo.samuderamanagementapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.softindo.samuderamanagementapp.Database.Const;
import com.softindo.samuderamanagementapp.Database.DBHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity {

    /* OK HTTP */
    OkHttpClient client;
    JSONObject jsonObject;


    /* Content */
    EditText et_username, et_password;
    Button btn_login;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_login = (Button) findViewById(R.id.btn_login);
        et_username = (EditText) findViewById(R.id.username);
        et_password = (EditText) findViewById(R.id.password);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = et_username.getText().toString();
                final String password = et_password.getText().toString();
                progress = ProgressDialog.show(Login.this, "Authenticating",
                        "Loading ..", true);
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                doLogin(username, password);
                            }
                        },
                        1000);
            }
        });

    }

    private void doLogin(String username, String password){
        client = new OkHttpClient();

        RequestBody requestBody = new FormBody.Builder()
                .add("email", username)
                .add("password", password)
                .build();

        final Request request = new Request.Builder()
                .url(Const.urlLogin)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code : " + response);
                } else {
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.dismiss();
//                                Toast.makeText(getApplicationContext(), jsonObject + "", Toast.LENGTH_LONG).show();
//                                Log.d("object", jsonObject + "");
                                try {
                                    if (jsonObject.getInt("status") == 1) {
                                        execSuccess(jsonObject);
                                    } else {
                                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void execSuccess(JSONObject jsonObject){
        Map<String, String> m_user = new HashMap<String, String>();
        try {
            Log.d("datauserna", jsonObject.toString());
            JSONObject data_user    = jsonObject.getJSONObject("data_user");

            //user
            Iterator<String> iter = data_user.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    Object value = data_user.get(key);
                    m_user.put(key, (String) String.valueOf(value));
                } catch (JSONException e) {
                    // Something went wrong!
                    Log.d("exception message", "something went wrong !");
                }
            }
            if(DBHandler.registerUser(getApplicationContext(), m_user)){
                Toast.makeText(getApplicationContext(), "Successfully Login", Toast.LENGTH_LONG).show();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                Intent a = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(a);
                                finish();
                            }
                        },
                        1000);
            }
            else{
                Toast.makeText(getApplicationContext(), "User has been logged in", Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
